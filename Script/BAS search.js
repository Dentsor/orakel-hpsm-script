javascript:(
function()
{
	/*
	| BAS Search - v0.0.5
	*/
	var usr = window.prompt("Username:");
	if(usr === null){
		console.log("BAS search cancelled!");
	}else if(usr != "") {
		window.open("https://bas.ntnu.no/account/search/?name="+usr, "_blank");
	}else{
		var nid = window.prompt("National Identity Number (11 digits):");
		
		if(nid.length == 12){
			nnid = nid.replace(" ", "");
			if(nnid.length == 11){
				nid = nnid;
			}
		}
		if(nid === null){
			console.log("BAS search cancelled!");
		}else if(nid != "" && nid.length == 11) {
			window.open("https://bas.ntnu.no/person/search/?name=&first_name=&last_name=&birth_date=&nin="+nid, "_blank");
		}else{
			var birthdate = window.prompt("Birth-date (YYYY-MM-DD):");
			
			if(birthdate === null){
				console.log("BAS search cancelled!");
			}else{
				var name = window.prompt("Full Name:");
				if(name === null){
					console.log("BAS search cancelled!");
				}else if(name != ""){
					window.open("https://bas.ntnu.no/person/search/?name="+name+"&first_name=&last_name=&birth_date="+birthdate+"&nin=", "_blank");
				}else{
					var firstname = window.prompt("First Name:");
					
					if(firstname === null){
						console.log("BAS search cancelled!");
					}else{
						var lastname = window.prompt("Last Name:");
						
						if(lastname === null){
							console.log("BAS search cancelled!");
						}else if(birthdate != "" || firstname != "" || lastname != ""){
							window.open("https://bas.ntnu.no/person/search/?name=&first_name="+firstname+"&last_name="+lastname+"&birth_date="+birthdate+"&nin=", "_blank");
						}else{
							console.log("BAS search cancelled. All fields empty!");
						}
					}
				}
			}
		}
	}
}
)()