javascript:(
	function()
	{
		function refreshList(time)
		{
			if (typeof refreshList_t !== "undefined") {
				clearInterval(refreshList_t);
			}

			if (time == "cancel") {
				console.log("Refreshlist has been cancelled!");
			} else {
				console.log("Refresh time set to: " + time + "ms");
				refreshList_t = setInterval(function(){
					var arr = document.querySelectorAll("button");
					for(i = 0; i < arr.length; i++) {
						if (arr[i].innerHTML == "Refresh") {
							arr[i].click();
						}
					}
				},time);
			}
		}

		var timeout = prompt("Set time (seconds):");
		if (timeout == null || timeout == "") { timeout = "cancel"; } else { timeout = timeout * 1000; }
		refreshList(timeout);
	}
)()