javascript:(
function()
{
	repetiveDispatch = {};
	while( key = prompt("Please define field name:") ){
		if (key.toLowerCase() == "help"){
			if (confirm("Do you want to open our support page?")){
				window.open("https://bitbucket.org/Dentsor/orakel-hpsm-script/wiki/RepetiveDispatcher-Help", "_blank");
				return;
			}
		}
		if (key.toLowerCase() == "help!") {
			window.open("https://bitbucket.org/Dentsor/orakel-hpsm-script/wiki/RepetiveDispatcher-Help", "_blank");
			return;
		}else{
			value = prompt("Value to be entered into the field '" + key + "':");
			if (value){
				repetiveDispatch[key] = value;
				if (typeof repetiveDispatch["length"] === "undefined"){
					repetiveDispatch["length"] = 1;
				}else{
					repetiveDispatch["length"]++;
				}
			}
		}
	}
}
)()