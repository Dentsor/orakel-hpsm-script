javascript:(
function()
{
	function loadScript(url, callback)
	{
		console.log("Loading script: " + url);

		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;

		script.onreadystatechange = callback;
		script.onload = callback;

		head.appendChild(script);
	}

	function jquery(callback)
	{
		if (!window.jQuery) {
			var url = "https://code.jquery.com/jquery-1.11.1.min.js";
			loadScript(url, callback);
		} else {
			callback();
		}
	}

	jquery(function() {
		var arr = document.querySelectorAll(".x-tab-panel-body .x-panel.x-panel-noborder");
		var validOrigin = (document.location.origin === "https://hit.it.ntnu.no");
		if(!validOrigin) { validOrigin = confirm("This script is made to only run on NTNUs HPSM\nDo you want to continue?"); }

		if(validOrigin) {
			for (var i=0; i < arr.length; i++){
				if(arr[i].id.substring(0, 3) == "ext"){
					if(arr[i].className.indexOf('x-hide-nosize') == -1){
						var arr2 = arr[i].querySelectorAll("iframe.ux-mif");
						if(arr2.length === 1) {
							var doc = arr2[0].contentWindow.document;
							var fields = {}, listButtons = {}, unknownButtons = {}, findButtons = {}, fillButtons = {}, actionButtons = {}, labels = {};

							labels["interactionId"] = $('label:contains("Interaction ID")', doc).first();
							labels["status"] = $('label:contains("Status")', doc).first();
							labels["helpdesk"] = $('label:contains("Helpdesk")', doc).first();
							labels["contact"] = $('label:contains("Contact")', doc).first();
							labels["contactEmail"] = $('label:contains("Contact email")', doc).first();
							labels["title"] = $('label:contains("Title")', doc).first();
							labels["endUser"] = $('label:contains("End user")', doc).first();
							labels["endUserDept"] = $('label:contains("End user dept.")', doc).first();
							labels["description"] = $('label:contains("Description")', doc).first();
							labels["category"] = $('label:contains("Category")', doc).first();
							labels["subcategory"] = $('label:contains("Subcategory")', doc).first();
							labels["service"] = $('label:contains("Service")', doc).first();
							labels["impact"] = $('label:contains("Impact")', doc).first();
							labels["urgency"] = $('label:contains("Urgency")', doc).first();
							labels["priority"] = $('label:contains("Priority")', doc).first();
							labels["assignmentGroup"] = $('label:contains("Assignment group")', doc).first();
							labels["assignee"] = $('label:contains("Assignee")', doc).first();
							labels["targetDate"] = $('label:contains("SLA target date")', doc).first();
							labels["newUpdate"] = $('label:contains("New update")', doc).first();
							labels["closureCode"] = $('label:contains("Closure code")', doc).first();
							labels["solution"] = $('label:contains("Solution")', doc).first();

							fields["interactionId"] = $("#"+labels["interactionId"].attr("for"), doc);
							fields["status"] = $("#"+labels["status"].attr("for"), doc);
							fields["helpdesk"] = $("#"+labels["helpdesk"].attr("for"), doc);
							fields["contact"] = $("#"+labels["contact"].attr("for"), doc);
							fields["contactEmail"] = $("#"+labels["contactEmail"].attr("for"), doc);
							fields["title"] = $("#"+labels["title"].attr("for"), doc);
							fields["endUser"] = $("#"+labels["endUser"].attr("for"), doc);
							fields["endUserDept"] = $("#"+labels["endUserDept"].attr("for"), doc);
							fields["description"] = $("#"+labels["description"].attr("for"), doc);
							fields["category"] = $("#"+labels["category"].attr("for"), doc);
							fields["subcategory"] = $("#"+labels["subcategory"].attr("for"), doc);
							fields["service"] = $("#"+labels["service"].attr("for"), doc);
							fields["impact"] = $("#"+labels["impact"].attr("for"), doc);
							fields["urgency"] = $("#"+labels["urgency"].attr("for"), doc);
							fields["priority"] = $("#"+labels["priority"].attr("for"), doc);
							fields["assignmentGroup"] = $("#"+labels["assignmentGroup"].attr("for"), doc);
							fields["assignee"] = $("#"+labels["assignee"].attr("for"), doc);
							fields["assigneeFull"] = $( "#X" + (parseInt(labels["assignee"].attr("for").replace("X", "")) + 1), doc );
							fields["targetDate"] = $("#"+labels["targetDate"].attr("for"), doc);
							fields["newUpdate"] = $("#"+labels["newUpdate"].attr("for"), doc);
							fields["closureCode"] = $("#"+labels["closureCode"].attr("for"), doc);
							fields["solution"] = $("#"+labels["solution"].attr("for"), doc);

							listButtons["status"] = $("#"+labels["status"].attr("for") + "Button", doc);
							listButtons["helpdesk"] = $("#"+labels["status"].attr("for") + "Button", doc);
							listButtons["impact"] = $("#"+labels["status"].attr("for") + "Button", doc);

							unknownButtons["contact"] = $( "#X" + (parseInt(labels["contact"].attr("for").replace("X", "")) + 1), doc );
							unknownButtons["endUser"] = $( "#X" + (parseInt(labels["endUser"].attr("for").replace("X", "")) + 1), doc );
							unknownButtons["service"] = $( "#X" + (parseInt(labels["service"].attr("for").replace("X", "")) + 1), doc );

							findButtons["service"] = $("#"+labels["status"].attr("for") + "FindButton", doc);

							fillButtons["contact"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["endUser"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["category"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["subcategory"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["service"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["assignmentGroup"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["assignee"] = $("#"+labels["status"].attr("for") + "FillButton", doc);
							fillButtons["closureCode"] = $("#"+labels["status"].attr("for") + "FillButton", doc);

							actionButtons["markAsSpam"] = $('button:contains("Mark as SPAM")', doc);
							actionButtons["newEmail"] = $('button:contains("New Email")', doc);
							actionButtons["fullCaseflow"] = $('button:contains(Full Case Flow)', doc);
							actionButtons["refreshHistory"] = $('button:contains(Refresh History)', doc);

							/*console.log(fields); console.log(listButtons); console.log(unknownButtons); console.log(findButtons); console.log(fillButtons); console.log(actionButtons); console.log(labels);*/

							/* ---------------------------------------------------------------- */

							if(fields["title"].val().substring(0,7) === "SMS fra")
							{
								
								if(fields["contact"].val() == 0 && fields["category"].val() == 0 && fields["service"].val() == 0 && fields["assignmentGroup"].val() == 0)
								{
									if(fields["description"].val().length <= 8)
									{
										fields["contact"].val(fields["description"].val());
										fields["category"].val("request for information");
										fields["service"].val("Brukeradministrasjon");
										fields["assignmentGroup"].val("skranke - gløshaugen (ntnu-it)");

										fillButtons["contact"].click();
									} else { alert("Contact name has too many characters!"); }
								} else {
									alert("Fields not empty, aborting!");
								}
								
							} else {
								alert("Not a SMS case, aborting!");
							}

							/* ---------------------------------------------------------------- */

						}
						else if(arr2.length <= 1) { alert("Contact admin.\nError code: 800"); }
						else if(arr2.length >= 1) { alert("Contact admin.\nError code: 801"); }
						else { alert("Contact admin.\nError code: 802"); }
					}
				}
			}
		} else {
			console.log("You are not on the correct webpage for this script to run.\nError code: 701");
		}
	});
}
)()

/*
|----------------------------------------------------------------
| HPSM JS Framework
|----------------------------------------------------------------
|
| Error codes:
|  701 => Script trying to run on another site, aborting
|  800 => Found 0 (or negative?) "active" iframe objects, can not continue!
|  801 => Found multiple "active" iframe objects, can not continue!
|  802 => Error, but neither 800 or 801
|
| Changelog:
|  v0.0.1
|   - Initial release
|  v0.0.2
|   - Error code 701 now comes in log instead of alert!
|  v0.0.3
|   - Added 'loadScript(url, callback)'
|   - Added 'jquery(callback)', loads jquery 1.11.1, and can be used in callback.
|  v0.0.4
|   - Updated to fit new IDs of fields, buttons and more.
|   - Added 'ReadOnly'-notes for readonly fields.
|   - Removed listButtons["urgency"], as it no longer exists
|   - Added fields["contactEmail"]
|   - Added fields["priority"] (readonly)
|  v0.0.5
|   - Updated to fit new IDs
|   - Added fillButtons["subcategory"]
|  v0.1.0
|   - Rewrote to get fields and buttons by name instead of ids (using labels)
|   - Now uses jquery
|  v0.1.1
|   - jQuery will now only be loaded the first time the script is run
|
*/
